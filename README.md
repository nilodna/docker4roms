# docker4roms - Docker image to run the Regional Ocean Modeling System - ROMS

Creating a docker image, based on ubuntu 16.04, to run Regional Ocean Modeling System - ROMS, under any version of linux.


This docker image was based on the one found on [MetOcean's repository](https://github.com/metocean/docker-roms-public), with some simplification
and adapted by @nilodna for all Hydrodynamics Lab fellows.


## Pre-requisites

- Install docker in your system. See [here](https://docs.docker.com/engine/installation/).

## Getting started

Clone this repository and then type in your command line:

```
sudo docker build . -t docker4roms -f /home/danilo/phd/learningROMS/repos/docker4roms/Dockerfile
```

To access creating a directory connecting host/guest, use:

```
docker run -v /media/danilo/Danilo/PHD/learning_ROMS/repos/docker4roms/projects/upwelling/DATA/:/home/lhico/projects/upwelling/DATA/ -ti docker4roms
```

To run your new application, create a new folder in projects, using the MY_APP
definition (e.g. your_application_name) name, with includes and header files, maybe with a shell script to build and run the model (see projects/upwelling as example) and run the code:

```
export MYAPP=your_application_name

docker run -v /media/danilo/Danilo/PHD/learning_ROMS/repos/docker4roms/upwelling/DATA/OUTPUT/:/home/lhico/projects/upwelling/DATA/OUTPUT/ -ti docker4roms -e roms_app=upwelling build
```
## Running in background

To run this container in background, type:

```
docker run --name=roms -v ${PWD}/projects/upwelling/DATA/OUTPUT/:/home/lhico/projects/upwelling/DATA/OUTPUT/ -it -d docker4roms
```

You can check if the container are running by listing all active containers (docker ps)
or just the last one created:

```
docker ps -l
```

Finally, we can run a simulation with

```
docker exec -it roms /bin/bash /home/lhico/projects/upwelling/run_upwelling.sh
```

# How this works ?

Inside the container, the folder structure is organized as:

```mermaid
graph LR
  A[Container] --> B[/home/lhico/: work dir];
  B --> C[projects/ directory];
  B --> D[roms_src/: ROMS source code];
  C --> E[upwelling/: testcase directory];
  C --> F[another testcase/];
  E --> G[DATA/];
  G --> H[INPUT/];
  G -- volume --> I[OUTPUT/];
  F --> J((...));  
```

In the host computer (e.g. your personal computer), we have to keep an organization similar:

```mermaid
graph LR
  A[HOST] --> B[$PWD];
  B --> C[projects/];
  C --> D[upwelling/: with .in, .sh, .h, etc];
  C --> E[another testcase/];
  D --> F[DATA/];
  F --> G[INPUT/];
  F -- volume --> H[OUTPUT/];
```
